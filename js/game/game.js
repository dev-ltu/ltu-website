var canvas, stage, exportRoot;
$(window).on('load', function () {
	// --- write your JS code here ---
	
	canvas = document.getElementById("canvas");
	exportRoot = new lib.nlo();

	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);
	stage.update();

	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
});

$(document).ready(function() {
	var playerStepX = 4;
	var enemiesCount = 12;
	var maxBulletsCount = 50;
	var isBot = false;
	var startTopBullet = 270;
	var bulletStep = 2;
	var delayForBotGame = 10000;
	var enemies = ['Angular', 'Css3', 'PHP', 'LINUX', 'WP', 'Java', 'UX', 'NODE.JS'];
	var bullets = [];
	var heightGameScreen = $('.game-screen').height();
	var time = 0;
	var turn = 1;
	var bot_interval;
	var timer = setTimeout(function () {
            console.log('bot');
            bot_interval = setInterval(function () {
                time += 50;
                var left = parseInt($('.game-screen .nlo').css('left')? $('.game-screen .nlo').css('left') : 0); 
                var half_width_screen = Math.round($('.game-screen').width() / 2);

                if ( left > half_width_screen ){
                  left -= playerStepX;
                  turn = -1;
                }else if( left < -half_width_screen ){
                  left += playerStepX;
                  turn = 1;
                }else{
                    left += playerStepX * turn;
                }

                $('.game-screen .nlo').css('left', left + 'px');

                if ( Math.random() >= 0.5 && (time % 1000 == 0) && (bullets.length < maxBulletsCount) ) {
					let bullet = document.createElement( "div" );
					bullet.className = "bullet";
					bullet.textContent = "|";
					left += 30;

					$(bullet).css({'left': left, 'top': startTopBullet});
					$('.bullets').append(bullet);

					bullets.push({'left': left, 'top': startTopBullet, 'obj' : bullet});
                }
            }, 50);
        },1000); 



	(function(){
		$('.game-screen .properties__list').html('');
		for (var i = 0; i < enemiesCount; i++) {
			let enemy_html = '<div class="properties__item">' + enemies[Math.floor(Math.random()*enemies.length)]; + '<span></span></div>';
		  	$('.game-screen .properties__list').append(enemy_html);
		}
		heightGameScreen = $('.game-screen').height();
	})();

	/***check out border***/
	function checkOut(val){
		var max_left_value = Math.round($('.game-screen').width() / 2);

		return (val > max_left_value) || (val < -max_left_value);
	} 

	/*** event keydown***/
	$(document).on('keydown', function(e) {
		$.fn.fullpage.setKeyboardScrolling(true);
    	if( $.fn.fullpage.getActiveSection().anchor == 'firstPage' ){
    		var left = parseInt($('.game-screen .nlo').css('left')? $('.game-screen .nlo').css('left') : 0); 
    		var top = parseInt($('.game-screen .nlo').css('top')? $('.game-screen .nlo').css('top') : 0); 
    		clearTimeout(timer);
            clearInterval(bot_interval);

    		if( e.which == 39 && !checkOut( left + playerStepX ) ){ // left arrow
    			e.preventDefault();
    			$('.game-screen .nlo').css('left', left + playerStepX + 'px');
    		}else if( e.which == 37 && !checkOut( left - playerStepX ) ){ // right arrow
    			e.preventDefault();
    			$('.game-screen .nlo').css('left', left - playerStepX + 'px');
    		} else if( e.which == 32 ){ // space button
    			e.preventDefault();
    			$.fn.fullpage.setKeyboardScrolling(false);

    			if( bullets.length < maxBulletsCount ){
    				let bullet = document.createElement( "div" );
    				bullet.className = "bullet";
    				bullet.textContent = "|";
    				left += 30;

    				$(bullet).css({'left': left, 'top': startTopBullet});
    				$('.bullets').append(bullet);

    				bullets.push({'left': left, 'top': startTopBullet, 'obj' : bullet});
    			}
    		}

    		timer = setTimeout(function () {
	            console.log('bot');
	            bot_interval = setInterval(function () {
	                time += 50;
	                var left = parseInt($('.game-screen .nlo').css('left')? $('.game-screen .nlo').css('left') : 0); 
	                var half_width_screen = Math.round($('.game-screen').width() / 2);

	                if ( left > half_width_screen ){
	                  left -= playerStepX;
	                  turn = -1;
	                }else if( left < -half_width_screen ){
	                  left += playerStepX;
	                  turn = 1;
	                }else{
	                    left += playerStepX * turn;
	                }

	                $('.game-screen .nlo').css('left', left + 'px');

	                if ( Math.random() >= 0.5 && (time % 1000 == 0) && (bullets.length < maxBulletsCount) ) {
						let bullet = document.createElement( "div" );
						bullet.className = "bullet";
						bullet.textContent = "|";
						left += 30;

						$(bullet).css({'left': left, 'top': startTopBullet});
						$('.bullets').append(bullet);

						bullets.push({'left': left, 'top': startTopBullet, 'obj' : bullet});
	                }
	            }, 50);
	        },3000);
    	}
	});
	/*** mouse move ***/
	$(document).on('mousemove', '.game-screen', function( event ) {
		event.preventDefault();
		clearTimeout(timer);
        clearInterval(bot_interval);
	  	var nlo_coord = event.clientX - $('.game-screen').position().left - Math.round($('.game-screen').width() / 2);

	  	if( !checkOut( nlo_coord ) ){
    		$('.game-screen .nlo').css('left', nlo_coord + 'px');
	  	}

	  	timer = setTimeout(function () {
            console.log('bot');
            bot_interval = setInterval(function () {
                time += 50;
                var left = parseInt($('.game-screen .nlo').css('left')? $('.game-screen .nlo').css('left') : 0); 
                var half_width_screen = Math.round($('.game-screen').width() / 2);

                if ( left > half_width_screen ){
                  left -= playerStepX;
                  turn = -1;
                }else if( left < -half_width_screen ){
                  left += playerStepX;
                  turn = 1;
                }else{
                    left += playerStepX * turn;
                }

                $('.game-screen .nlo').css('left', left + 'px');

                if ( Math.random() >= 0.5 && (time % 1000 == 0) && (bullets.length < maxBulletsCount) ) {
					let bullet = document.createElement( "div" );
					bullet.className = "bullet";
					bullet.textContent = "|";
					left += 30;

					$(bullet).css({'left': left, 'top': startTopBullet});
					$('.bullets').append(bullet);

					bullets.push({'left': left, 'top': startTopBullet, 'obj' : bullet});
                }
            }, 50);
        },3000);
	});

	$(document).on('mousedown', '.game-screen', function( event ) {
		event.preventDefault();
		clearTimeout(timer);
        clearInterval(bot_interval);

	  	if( (event.which == 1) && (bullets.length < maxBulletsCount) ){
    		let left = parseInt($('.game-screen .nlo').css('left')? $('.game-screen .nlo').css('left') : 0); 
			let bullet = document.createElement( "div" );
			bullet.className = "bullet";
			bullet.textContent = "|";
			left += 30;

			$(bullet).css({'left': left, 'top': startTopBullet});
			$('.bullets').append(bullet);

			bullets.push({'left': left, 'top': startTopBullet, 'obj' : bullet});
	  	}

	  	timer = setTimeout(function () {
            console.log('bot');
            bot_interval = setInterval(function () {
                time += 50;
                var left = parseInt($('.game-screen .nlo').css('left')? $('.game-screen .nlo').css('left') : 0); 
                var half_width_screen = Math.round($('.game-screen').width() / 2);

                if ( left > half_width_screen ){
                  left -= playerStepX;
                  turn = -1;
                }else if( left < -half_width_screen ){
                  left += playerStepX;
                  turn = 1;
                }else{
                    left += playerStepX * turn;
                }

                $('.game-screen .nlo').css('left', left + 'px');

                if ( Math.random() >= 0.5 && (time % 1000 == 0) && (bullets.length < maxBulletsCount) ) {
					let bullet = document.createElement( "div" );
					bullet.className = "bullet";
					bullet.textContent = "|";
					left += 30;

					$(bullet).css({'left': left, 'top': startTopBullet});
					$('.bullets').append(bullet);

					bullets.push({'left': left, 'top': startTopBullet, 'obj' : bullet});
                }
            }, 50);
        },3000);
	});

	/*** render game every 100ms ***/
	function gameRender(){
		var new_bullets = [];
		for (i = 0; i < bullets.length; i++) { 
		  let bullet = bullets[i];
		  let top = bullet.top + bulletStep;
		  
		  $(bullet.obj).css({'top': top});
		  bullet.top = top;

		  let checkCollision = checkCollisionBullet(bullet.obj);

		  if( checkCollision ){
		  	$(bullet.obj).remove();
		  }else{
		  	new_bullets.push(bullet);
		  }
		}

		bullets = new_bullets;
	}
	setInterval(gameRender, 50);

	/*** check collision bullet with enemies and border ***/
	function checkCollisionBullet(obj){
		let top = parseInt($(obj).css('top')? $(obj).css('top') : 0); 
		let collision_flag = false
		/***out border***/
		if( top > heightGameScreen){
			return true;
		}

		/***collision with enemy***/
		$( ".properties__item" ).each(function() {
			var relativeY = $(this).offset().top - $(obj).offset().top;
			var relativeX = $(this).offset().left - $(obj).offset().left;

		  	if( relativeY < 22  && relativeX < 0 && relativeX > -104 && !collision_flag ){
		  		$(this).addClass('active').fadeOut(500);
		  		collision_flag = true;
		  	}
		});
			
		return collision_flag;
	}

});

