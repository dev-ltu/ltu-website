$(document).ready(function() {
	$('#fullpage').fullpage({
		anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourPage'],
		menu: "#menu",

		afterLoad: function(origin, destination, direction){
			if( direction == 'down' ){
				if( destination.anchor == 'thirdPage' ){
					$('.header').addClass('dark');
				}else{
					$('.header').removeClass('dark');
				}
			}
		},
		onLeave: function(origin, destination, direction){
			if( destination.anchor == 'thirdPage' ){
				$('#fp-nav').addClass('black');
			}else{
				$('#fp-nav').removeClass('black');
			}
			
			if( direction == 'up' ){
				if( destination.anchor == 'thirdPage' ){
					$('.header').addClass('dark');
				}else{
					$('.header').removeClass('dark');
				}
			}

		},
		navigation: true,
		slidesNavigation: true,
		scrollOverflow: true,
	});

	$('.footer-section__btn').click(function(e){
		e.preventDefault();
		$.fn.fullpage.moveSectionDown();
	});

	/*form*/
	$('.form__input, .form__textarea').on('change',function(){
		if($(this).val() != '') {
			$(this).addClass('not-empty');
		} else {
			$(this).removeClass('not-empty');
		}
	});



	/*switch*/
	$('.switch').change(function(){
		$('.sky-cloud').removeClass('show-part-left');
		$('.particles-js').removeClass('show-part-right');
		$('.particles-js').removeClass('show-part-left');
		$('.sky-cloud').removeClass('show-part-right');
					
		if ($(this).attr('data-switch') == 'black') {
			$(this).attr('data-switch', 'sky');
			$('.sky-cloud').addClass('show');
			$('.particles-js').removeClass('show');
			$('.particles-js').addClass('show-part-left');
			$('.sky-cloud').addClass('show-part-right');
		} else {
			$(this).attr('data-switch', 'black');
			$('.sky-cloud').removeClass('show');
			$('.particles-js').addClass('show');
			$('.sky-cloud').addClass('show-part-left');
			$('.particles-js').addClass('show-part-right');
		}
	});

	$('body').on('mouseover', '.switch', function(e){
		if ($(this).attr('data-switch') == 'black') {
			$('.sky-cloud').addClass('show-part-left');
			$('.particles-js').addClass('show-part-right');
		} else {
			$('.particles-js').addClass('show-part-left');
			$('.sky-cloud').addClass('show-part-right');
		}
	});

	$('body').on('mouseleave', '.switch', function(e){

			$('.sky-cloud').removeClass('show-part-left');
			$('.particles-js').removeClass('show-part-right');
			$('.particles-js').removeClass('show-part-left');
			$('.sky-cloud').removeClass('show-part-right');
	});


	/*menu*/
	$('.header__btn').click(function(){
		if ($(this).attr('data-open') =='close') {
			$(this).attr('data-open', 'open');
			$('.menu').addClass('active');
			$('body').addClass('noScrolling');
		}
	})

	$('.menu__close, .menu__link').click(function(){
		$('.header__btn').attr('data-open', 'close');
		$('.menu').removeClass('active');
		$('body').removeClass('noScrolling');
	})


	/*slider*/
	function showDrumPreview(dataValue) {
		$(".clients__logo div.preview").each(function(){
			var previewValue = $(this).data('value');
			if (previewValue == dataValue) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	}

	$(".clients__slider-wrap").drum({ 
		panelCount: 30,
		dail_w: 0, 
	  	dail_h: 0, 
		onChange : function (e) {
			showDrumPreview(e.value);
		} 
	});

	$(".clients__slider-prev").click(function() {
		var nextIndex = $('.clients__slider-wrap option:selected').prev().index();
		nextIndex = parseInt(nextIndex);
		if (nextIndex == -1) {
			nextIndex = $('.clients__slider-wrap option').last().index();
		}
		$(".clients__slider-wrap").drum('setIndex', nextIndex);
		showDrumPreview($(".clients__slider-wrap").val());
	});

	$(".clients__slider-next").click(function() {
		var nextIndex = $('.clients__slider-wrap option:selected').next().index();
		nextIndex = parseInt(nextIndex);
		if (nextIndex == -1) {
			nextIndex = 0
		}
		$(".clients__slider-wrap").drum('setIndex', nextIndex);
		showDrumPreview($(".clients__slider-wrap").val());
	});

	showDrumPreview($("#country").val());

	/*service set active on hover*/
	$('body').on('mouseover', '.work__item', function(e){
		e.preventDefault();
		$('.work__item').removeClass('active');
		$(this).addClass('active');
	});

});

$(window).on('load', function() {
	setTimeout(function(){
		$('body').removeClass('hidden-scroll');
		$('.site-preload').remove();
	}, 1000);
});